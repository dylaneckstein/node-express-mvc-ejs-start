const express = require('express')
const api = express.Router()
var app = express()
api.use(express.static(__dirname + '/img/'));

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET t11
api.get('/t11', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t11/index.ejs',
        { title: 'TeamName', layout: 'layout.ejs' })
})
api.get('/t11/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t11/a/index.ejs',
        { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
})
api.get('/t11/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t11/b/index.ejs',
        { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
})
api.get('/t11/c', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t11/c/index.ejs',
        { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
})
// GET t15 - mentors
api.get('/t15', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t15/index.ejs',
        { title: 'Mentors', layout: 'layout.ejs' })
})
api.get('/t15/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t15/e/index.ejs',
        { title: 'Rathnakar Ettedi', layout: 'layout.ejs' })
})
api.get('/t15/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t15/r/index.ejs',
        { title: 'Vamsi Ravva', layout: 'layout.ejs' })
})

module.exports = api
