/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  res.render('index.ejs', { title: 'Express App' })
})

router.get('/about/t11/a', (req, res, next) => {
  LOG.debug('Request to /about/t11/a')
  res.render('about/t11/a/index.ejs', { title: 'Elizabeth' })
})


// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/pilot', require('../controllers/pilot.js'))
router.use('/flight', require('../controllers/flight.js'))
router.use('/plane', require('../controllers/plane.js'))

LOG.debug('END routing')
module.exports = router
